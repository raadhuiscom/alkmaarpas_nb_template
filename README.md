# Alkmaar Marketing email templates

Alkmaar Marketing templates with Yeoman Ink Me generator https://github.com/dnnsldr/yeoman-ink-me-generator

## Installation

Run `npm install` to install node packages

Bower install: `bower install` to install dependencies

## Using LiveReload function

To get started you can run `grunt` from the terminal. 

The default "grunt" will start the `watch` process for all css files and the index.html file. LiveReload is now enabled. 
Grab the [Chrome extension](https://chrome.google.com/webstore/detail/livereload/jnihajbhpnppcggbcgedagnkighmdlei) for LiveReload to help with ports.

## When Your Ready to Finalize the Files for Production
## Final Build

Once you are ready to package up your final build, grunt will get rid of the unused css from Ink and inline all the css, ftp your images to your image hosting provider, and run a Litmus test if you chose Litmus -

* run the command `grunt inkify` from the terminal

* This will create a copy of your index file with a reference to a newly created css file. This new css file gets rid of all of the unused css from Ink and makes a copy of the new css for reference in the copied html.

* The grunt task of 'premailer' will take the newly reference html and css and move all css inline.

* Images will be optimized

* Images will be FTP'd to your hosting provider. 

* Image relative URL's will be replaced with absolute URL's from Image Hosting Domain set during the generator.

* A test will be sent to Litmus if you choose to with the clients you define.

* Thats it. There will be a new folder called 'dist' that will have your `email-inline.html` file that is ready for use.

## Tips

* To get out of the 'Watch' when running the terminal, on MacOS click `control` and `c`. This will get your terminal back to the prompt to run new tasks.

* Make all CSS changes in the `style.css`. The `ink.css` is from the bower componenet and is pulling the lastest ink css. These files will be merged togehter and will get rid of any unused when running `grunt inkify`.

##
