module.exports = {
	main: {
    	options: {
      		verbose: true,
      		removeComments: true
    	},
    	files: {
      		'dist/alkmaarpas-inline.html': ['dist/alkmaarpas.html']
    	}
  	}
};