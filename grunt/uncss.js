module.exports = {
    dist: {
        src: ['alkmaarpas.html'],
        dest: 'dist/css/tidy.css',
        options: {
            report: 'min' // optional: include to report savings
        }
    }
};